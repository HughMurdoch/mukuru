/*
Filename: dhx.js
Author  : DC@YBS
*/

// specify requirements/dependencies here please
// requires cLog() from common.js

/* Common */
// turns an obj into an encoded URL
function obj2URL(obj) {
	var str = "";
	for (var key in obj) {
		if (str != "") {str += "&";}
		str += key + "=" + encodeURIComponent(obj[key]);
	}
	return str;
}

/* Ajax */
// Asynchronous (will NOT wait for response) GET call via dHTMLx's Ajax
// call fnSuccess on success
// call fnError on error
function dhxAjaxGet(URL, fnSuccess, fnError) {
	cLog('dhxAjaxGet(URL='+URL+')');
	window.dhx4.ajax.get(URL, function(r){
		cLog('r='+r.xmlDoc.responseText);
		// obj = JSON.parse(r.xmlDoc.responseText);
		var obj = window.dhx4.s2j(r.xmlDoc.responseText);
		cLog('\\/ obj \\/');
		cLog(obj);
		if (obj != null && obj.status == 'ok') {
			dhtmlx.message({text: obj.message, expire:2000})
			if (typeof fnSuccess !== 'undefined')
				fnSuccess();
		}
		else {
			dhtmlx.message({title: "Error",	type:"alert-error",	text:obj.message});
			if (typeof fnError !== 'undefined')
				fnError();
		}
	});	
}

// Synchronous (will wait for response) GET call via dHTMLx's Ajax
// returns true or false
function dhxAjaxGetSync(URL) {
	cLog('dhxAjaxGetSync(URL='+URL+')');
	var r = window.dhx4.ajax.getSync(URL);
	cLog('r='+r.xmlDoc.responseText);
	var obj = window.dhx4.s2j(r.xmlDoc.responseText);
	cLog('\\/ obj \\/');
	cLog(obj);
	if (obj != null && obj.status == 'ok') {
		dhtmlx.message({text: obj.message, expire:2000})
		return true;
	}
	else {
		dhtmlx.message({title: "Error",	type:"alert-error",	text:obj.message});
		return false;
	}
}

// Synchronous (will wait for response) GET call via dHTMLx's Ajax
// returns an object
function dhxAjaxGetObj(URL) {
	cLog('dhxAjaxGetObj(URL='+URL+')');
	var r = window.dhx4.ajax.getSync(URL);
	cLog('r='+r.xmlDoc.responseText);
	var obj = window.dhx4.s2j(r.xmlDoc.responseText);
	cLog('\\/ obj \\/');
	cLog(obj);
	return obj;
}

/* dhxForm */
// Params: provide dhxForm object
// Return: JSON string of that form
function dhxForm_Data2JSON(dhxForm) {
	cLog('dhxForm_Data2JSON');
	cLog(dhxForm.getFormData());
	return JSON.stringify(dhxForm.getFormData());
}

// Params: provide dhxForm object
// Return: encoded URL string
function dhxForm_Data2Param(dhxForm) {
	return obj2URL(dhxForm.getFormData());
}

// validate given form object
// returns true/false, also gives error message on error
function dhxForm_Validate(dhxForm) {
	cLog('dhxForm_validate(dhxForm)');
	cLog(dhxForm);
	
	// check all combo's
	/*dhxForm.forEachItem(function(name){
		// cLog(name);
		// cLog(dhxForm.getItemType(name));
		if (dhxForm.getItemType(name) == 'combo') {
			cLog(dhxForm.getCombo(name).confirmValue());
		}
	});*/
	
	if (dhxForm.validate()) {
		return true;
	}
	else {
		dhxMsgError("Form Validation Error", "Please check that all required fields are completed and have valid values");
		return false;
	}
}

/* dhxMsg */
function dhxMsg(text) {
	dhtmlx.message({
		text:text
	});	
}

function dhxMsgAlert(title, text) {
	dhtmlx.message({
		title:title,
		type:"alert",
		text:text,
	});	
}

function dhxMsgError(title, text) {
	dhtmlx.message({
		title:title,
		type:"alert-error",
		text:text
	});	
}

function dhxMsgWarn(title, text) {
	dhtmlx.message({
		title:title,
		type:"alert-warning",
		text:text
	});	
}

/* dhxWin */
function dhxWinStart(skin) {
	if (typeof dhxWin == 'undefined') {
		var dhxWin;
	}
	
	dhxWin = new dhtmlXWindows();
	dhxWin.setSkin(skin);
	// dhxWin.attachViewportTo(document.body);
	// dhxWin.enableAutoViewport();
	
	return dhxWin;
}

