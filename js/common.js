// common.js

// write given text to the console.log
// one common functin allows me to control this with a debug switch, and also write to a log file for download later
function cLog(txt) {
	if (typeof gLogToConsole !== 'undefined' && gLogToConsole == 1)
		console.log(txt);
}

// log a function call with its arguments
function fLog(arguments) {
	clog('Func: ' + arguments.callee.name);
	clog('Args: ' + arguments);
}

// custom empty()
function empty(v) {
	if (v == null)
		return true;
	
	if (v == 0)
		return true;

	if (v == '0')
		return true;

	if (v == '')
		return true;
	
	if (v == 'null')
		return true;
	
	return false;
}


