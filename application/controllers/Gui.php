<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gui extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->database();
	}

	public function index() {
		$this->load->view('v_header');
		$this->load->view('v_api');
		$this->load->view('v_footer');
	}
}