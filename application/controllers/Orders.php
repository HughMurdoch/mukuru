<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}

	public function index() {
        log_message('debug','CONTROLLER: orders/index');
        $this->load->model('Order_model');
        $all_orders = $this->Order_model->get_all();
        foreach($all_orders as $order) {
            $ar_orders[] = array('Currency' => $order->Currency, 'Rate' => $order->Rate, 'Surcharge' => $order->Surcharge, 'AmountPurchase' => $order->AmountPurchase, 'AmountPay' => $order->AmountPay, 'AmountSurcharge' => $order->AmountSurcharge, 'AmountDiscount' => $order->AmountDiscount, 'dtCreated' => $order->dtCreated);
        }

        $data = array('ar_orders' => $ar_orders);

        $this->load->view('v_header');
        $this->load->view('v_orders', $data);
        $this->load->view('v_footer');
	}

    public function orders_get_all() {
        $this->load->model('Order_model');
        $data = $this->Order_model->get_all();
        print_r($data);
    }
}