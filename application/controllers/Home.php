<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}

	public function index() {
		log_message('debug','CONTROLLER: home/index');
		$this->load->model('Rate_model');
        $all_rates = $this->Rate_model->get_all();

		foreach($all_rates as $rate) {
			$ar_rates[$rate->Code] = array('Description' => $rate->Description, 'Rate' => $rate->Rate);
		}
		
		$data = array('ar_rates' => $ar_rates);

		$this->load->view('v_header');
		$this->load->view('v_home', $data);
		$this->load->view('v_footer');
	}

	public function order_insert() {
		$this->load->model('Rate_model');
    	$this->load->model('Order_model');
		
		$Currency = $this->Rate_model->get($this->input->get('Currency'));
	
		$data = array(
			'Currency' => $this->input->get('Currency'),
			'Rate' => $Currency->Rate,
			'Surcharge' => $Currency->Surcharge,
			'AmountPurchase' => $this->input->get('AmountPurchase'),
			'AmountPay' => $this->input->get('AmountPurchase') / $Currency->Rate,
			'AmountSurcharge' => ($this->input->get('AmountPurchase') / $Currency->Rate) * ($Currency->Surcharge / 100),
			'AmountDiscount' => 0,
		);
				
		switch ($data['Currency']) {
			case 'GBP':
				$this->load->library('email');

				$this->email->to('someone@example.com');

				$this->email->from('your@example.com', 'Your Name');
				$this->email->cc('another@another-example.com');

				$this->email->subject('Order Created');
				$this->email->message('An order was created');

				$this->email->send();
			break;
			
			case 'EUR':
				$data['AmountDiscount'] = $data['AmountPay'] * ($Currency->Discount/100);
			break;
		}

		$this->Order_model->insert($data);
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode(array('status' => 'ok', 'message' => 'Order added')));
	}
	
	
}