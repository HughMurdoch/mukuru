<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}

	public function index() {
		log_message('debug','CONTROLLER: home/index');
		$this->load->model('Rate_model');
        $all_rates = $this->Rate_model->get_all();

		foreach($all_rates as $rate) {
			$ar_rates[$rate->Code] = array('Description' => $rate->Description, 'Rate' => $rate->Rate);
		}
		
		$data = array('ar_rates' => $ar_rates);

		$this->load->view('v_header');
		$this->load->view('v_database', $data);
		$this->load->view('v_footer');
	}
}