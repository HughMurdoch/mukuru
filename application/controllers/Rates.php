<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rates extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}

	public function index() {
		log_message('debug','CONTROLLER: rates/index');
		$this->load->model('Rate_model');
		$all_rates = $this->Rate_model->get_all();
		
		foreach($all_rates as $rate) {
			$ar_rates[$rate->Code] = array('Description' => $rate->Description, 'Rate' => $rate->Rate, 'Surcharge' => $rate->Surcharge, 'Discount' => $rate->Discount);
		}
		
		$data = array('ar_rates' => $ar_rates);

		$this->load->view('v_header');
		$this->load->view('v_rates', $data);
		$this->load->view('v_footer');
	}

	public function seedinitial() {
        $this->load->model('Rate_model');
        $this->Rate_model->seedinitial();
    }

    public function cleartable() {
        $this->load->model('Rate_model');
        $this->Rate_model->cleartable();
    }

    public function rate_insert() {
        $this->load->model('Rate_model');

        $data = array(
            'Code' => $this->input->get('Code'),
            'Description' => $this->input->get('Description'),
            'Rate' => $this->input->get('Rate'),
            'Surcharge' => $this->input->get('Surcharge'),
            'Discount' => $this->input->get('Discount')
        );


        $this->Rate_model->insert($data);

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode(array('status' => 'ok', 'message' => 'Order added')));
    }

    public function rates_get_all() {
        $this->load->model('Rate_model');
        $data = $this->Rate_model->get_all();
        print_r($data);
    }
}