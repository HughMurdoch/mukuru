<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}

    public function index() {
        log_message('debug','CONTROLLER: api/index');
        $this->load->view('v_header');
        $this->load->view('v_api');
        $this->load->view('v_footer');
    }

	public function rate_get() {
        if (!$id = $this->get('id')) {
        	$this->response(NULL, 400);
        }

    	$this->load->model('Rate_model');
		$res = $this->Rate_model->get($id);
		
        if($res) {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else {
            $this->response(array('error' => 'Record(s) not found'), 404);
        }
    }
	
	public function rates_get() {
    	$this->load->model('Rate_model');
		$res = $this->Rate_model->get_all();
		
        if($res) {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else {
            $this->response(array('error' => 'Record(s) not found'), 404);
        }
    }
	
	public function order_post() {
		$err = false;
		
		$Currency = $this->post('Currency');
		$AmountPurchase = $this->post('AmountPurchase');

    	$this->load->model('Rate_model');
		$this->Rate_model->get($Currency);
		
		$Rate = $this->Rate_model->Rate;
		$Surcharge = $this->Rate_model->Surcharge;
		$AmountPay = $AmountPurchase / $Rate;
		$AmountSurcharge = $AmountPay * $Surcharge;
		
		if (!$err) {
			$this->load->model('Order_model');

			$data = array(
			   'Currency' => $Currency,
			   'Rate' => $Rate,
			   'Surcharge' => $Surcharge,
			   'AmountPurchase' => $AmountPurchase,
			   'AmountPay' => $AmountPay,
			   'AmountSurcharge' => $AmountSurcharge,
			);
			
			if (!$this->Order_model->insert($data)) {
				$err = true;
				$message = $this->db->_error_message();
			}
			else {
				$message = "The record created successfully.";
			}
		}
		
		if ($err) {
			$ret = array(
				'status' => 'error',
				'message' => $message,
			);
		}
		else {
			$ret = array(
				'status' => 'ok',
				'message' => $message,
			);
		}
		$this->response($ret, 200); // 200 being the HTTP response code
	}
	
}

function isJson($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}