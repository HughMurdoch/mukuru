<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//--------------------------------------------------
// JS, CSS
if ( !function_exists('js') ) {
	function js($src) {
		return "<script type=\"text/javascript\" charset=\"utf-8\" src=\"$src\"></script>\n";
	}
}

if ( !function_exists('css') ) {
	function css($href) {
		return "<link rel=\"stylesheet\" type=\"text/css\" href=\"$href\">\n";
	}
}

//--------------------------------------------------
// Suite
if ( !function_exists('dhx_suite') ) {
	function dhx_suite() {
		echo js(base_url('dhx51/codebase/dhtmlx.js'));
		echo js(base_url('dhx51/codebase/dhtmlx_deprecated.js'));

		echo css(base_url('dhx51/skins/terrace/dhtmlx.css'));
	}
}

// Imgs
if ( !function_exists('dhx_imgs') ) {
	function dhx_imgs($lib, $skin='') {
		$lib_path = '';
		switch ($lib) {
			case 'wins'   	: $lib_path = 'dhxwins_terrace';	break;
			case 'layout' 	: $lib_path = 'dhxlayout_terrace';	break;
			case 'sidebar'	: $lib_path = 'dhxsidebar_terrace';	break;
			case 'form'   	: $lib_path = 'dhxform_terrace';	break;
		}
		echo base_url('dhx51/skins/terrace/imgs/'.$lib_path) . '/';
	}
}


//--------------------------------------------------
// Common
if ( !function_exists('dhx_common') ) {
	function dhx_common() {
		echo "<!--dhx_common-->\n";
		echo js(base_url('dhx51/sources' . '/dhtmlxCommon/codebase' . '/connector.js'));
		echo js(base_url('dhx51/sources' . '/dhtmlxCommon/codebase' . '/dhtmlxcommon.js')); // Only used for Ajax
		echo js(base_url('dhx51/sources' . '/dhtmlxCommon/codebase' . '/dhtmlxcontainer.js'));
		echo js(base_url('dhx51/sources' . '/dhtmlxCommon/codebase' . '/dhtmlxcore.js'));
		echo js(base_url('dhx51/sources' . '/dhtmlxCommon/codebase' . '/dhtmlxdataprocessor.js'));
	}
}

//--------------------------------------------------
// Windows
if ( !function_exists('dhx_windows') ) {
	function dhx_windows($skin) {
		echo "<!--dhx_windows-->\n";
		echo js(base_url('dhx51/sources' . '/dhtmlxWindows/codebase' . '/dhtmlxwindows.js'));
		echo js(base_url('dhx51/sources' . '/dhtmlxWindows/codebase' . '/dhtmlxwindows_deprecated.js'));
		
		// $skin = empty($skin) ? 'dhx_terrace' : $skin;
		echo css(base_url('dhx51/sources'. '/dhtmlxWindows/codebase' . '/skins/dhtmlxwindows_dhx_terrace.css'));
	}
}

if ( !function_exists('dhx_windows_imgs') ) {
	function dhx_windows_imgs($skin) {
		// $skin = empty($skin) ? 'dhx_terrace' : $skin;
		echo base_url('dhx51/sources' . '/dhtmlxWindows/codebase' . '/imgs/dhxwins_terrace') . '/';
	}
}

//--------------------------------------------------
// Layout
if ( !function_exists('dhx_layout') ) {
	function dhx_layout($skin) {
		echo "<!--dhx_layout-->\n";
		echo js(base_url('dhx51/sources' . '/dhtmlxLayout/codebase' . '/dhtmlxlayout.js'));
		echo js(base_url('dhx51/sources' . '/dhtmlxLayout/codebase' . '/dhtmlxlayout_deprecated.js'));
	
		// $skin = empty($skin) ? 'dhx_terrace' : $skin;
		echo css(base_url('dhx51/sources' . '/dhtmlxLayout/codebase' . '/skins/dhtmlxlayout_dhx_terrace.css'));
	}
}

if ( !function_exists('dhx_layout_imgs') ) {
	function dhx_layout_imgs($skin) {
		// $skin = empty($skin) ? 'dhx_terrace' : $skin;
		echo base_url('dhx51/sources' . '/dhtmlxLayout/codebase' . '/imgs/dhxlayout_terrace') . '/';
	}
}

//--------------------------------------------------
// Message
if ( !function_exists('dhx_message') ) {
	function dhx_message($skin) {
		echo "<!--dhx_message-->\n";
		echo js(base_url('dhx51/sources' . '/dhtmlxMessage/codebase' . '/dhtmlxmessage.js'));

		// $skin = empty($skin) ? 'dhx_terrace' : $skin;
		echo css(base_url('dhx51/sources' . '/dhtmlxMessage/codebase' . '/skins/dhtmlxmessage_dhx_terrace.css'));
	}
}

if ( !function_exists('icons') ) {
	function icons() {
		echo base_url('imgs/essential-set/png') . '/';
	}
}