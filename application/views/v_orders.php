<body id="page1" onload="doOnLoad()">
<div class="main">
    <!-- header -->
    <header>
        <div class="wrapper">
            <h1><a href="index.html" id="logo">Smart Biz</a></h1>
            <form id="search" action="" method="post">
                <div class="bg">
                    <input type="submit" class="submit" value="">
                    <input type="text" class="input">
                </div>
            </form>
        </div>
        <nav>
            <ul id="menu">
                <li class="alpha"><a href="<?=base_url('index.php/home');?>"><span><span>Forex</span></span></a></li>
                <li><a href="<?=base_url('index.php/rates');?>"><span><span>Rates</span></span> </a></li>
                <li id="menu_active"><a href="<?=base_url('index.php/orders');?>"><span><span>Orders</span></span></a></li>
                <li><a href="<?=base_url('index.php/database');?>"><span><span>Database</span></span></a></li>
                <li class="omega"><a href="<?=base_url('index.php/gui');?>"><span><span>API</span></span></a></li>
            </ul>
        </nav>
        <div class="wrapper">
            <div class="text">
                <span class="text1">Effective<span>business solutions</span></span>
                <a href="#" class="button">read more</a>
            </div>
        </div>
    </header>
    <!-- / header -->
    <!-- content -->
    <section id="content">
        <div class="wrapper">
            <div class="wrapper">
                <ul class="banners">
                    <li>
                        <a href="#"><img src="../images/page1_img1.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Company History</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img2.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Our Capabilities</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img3.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Where We Deliver</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img4.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Operations Consulting</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="wrapper">
                <div class="box bot pad_bot2">
                    <div class="pad">
                        <article>
                            <h2>Orders</h2>
                            <div id="forexrates">
                                <table>
                                    <thead>
                                        <th>
                                            Currency
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Surcharge
                                        </th>
                                        <th>
                                            Amount Purchased
                                        </th>
                                        <th>
                                            Amount Paid
                                        </th>
                                        <th>
                                            Amount Surcharge
                                        </th>
                                        <th>
                                            Amount Discount
                                        </th>
                                        <th>
                                            Date Created
                                        </th>
                                    </thead>
                                <?php
                                    foreach($ar_orders as $code => $order) {
                                        echo "\n<tr>\n";
                                        echo "  <td>\n";
                                        echo        $order["Currency"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["Rate"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["Surcharge"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["AmountPurchase"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["AmountPay"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["AmountSurcharge"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["AmountDiscount"];
                                        echo "  </td>\n";
                                        echo "  <td>\n";
                                        echo        $order["dtCreated"];
                                        echo "  </td>\n";
                                        echo "</tr>\n";
                                    }
                                ?>
                                </table>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / content -->
</div>
<script type="text/javascript"> Cufon.now(); </script>
</body>
