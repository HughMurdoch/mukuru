<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Mukuru Practical - Home</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=base_url('css/reset.css');?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?=base_url('css/layout.css');?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?=base_url('css/style.css');?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?=base_url('dhx51/codebase/dhtmlx.css');?>" type="text/css" media="all">
    <script type="text/javascript" src="<?=base_url('js/jquery-1.4.2.js');?>" ></script>
    <script type="text/javascript" src="<?=base_url('js/cufon-yui.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('js/cufon-replace.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('js/Myriad_Pro_400.font.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('js/Myriad_Pro_700.font.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('js/Myriad_Pro_600.font.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('js/common.js');?>" ></script>
    <script type="text/javascript" src="<?=base_url('js/dhx.js');?>" ></script>
    <script type="text/javascript" src="<?=base_url('dhx51/codebase/dhtmlx.js');?>" ></script>
    <script type="text/javascript" src="<?=base_url('dhx51/codebase/dhtmlx_deprecated.js');?>" ></script>
    <script type="text/javascript" charset="utf-8">
// global vars
var gURL, gLogToConsole = <?php echo (empty($_GET['log']) ? 0 : 1);?>;
</script>
<?
dhx_suite();
?>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            border-radius: 8px !important;
            -moz-border-radius: 5px !important;
        }

        th, td {
            text-align: left;
            padding: 8px !important;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #2e94d8;
            color: white;
        }

        table th:first-child {
            -moz-border-radius: 5px 0 0 0 !important;
        }
        table th:last-child {
            -moz-border-radius: 0 5px 0 0 !important;
        }
        table tr:last-child td:first-child {
            -moz-border-radius: 0 0 0 5px !important;
        }
        table tr:last-child td:last-child {
            -moz-border-radius: 0 0 5px 0 !important;
        }
    </style>
</head>
