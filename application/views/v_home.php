<script type="text/javascript" charset="utf-8">
    var dhxLayout, a;
    var dhxForm;

    var gCurrencies =<?php echo json_encode($ar_rates);?>;

    dhtmlxEvent(window, "load", function(){
        cLog('window.load');

        dhxLayout_init();
    })


    function dhxLayout_init() {
        cLog('dhxLayout_init()');

        dhxLayout = new dhtmlXLayoutObject({
            parent:"forexorder",
            pattern:"1C",
            offsets: {top: 0, right: 0, bottom: 0, left: 0},
            cells: [
                {
                    id: "a",
                    text: "Purchase Foreign Currency",
                    collapse: false,
                    header: true,
                },
            ]
        });

        a = dhxLayout.cells('a');

        dhxLayout.setSizes();

        dhxForm_init();
    }

    function dhxForm_init() {
        cLog('dhxForm_init()');

        var formStructure = [
            {type: "settings", position: "label-left", labelWidth: "200", inputWidth: "200", offsetLeft:20},
            {type: "select", label: "Currency", name: "selCurrency", options:[
                    {text: "Select currency", value: "", selected: true},
                    <?php foreach($ar_rates as $code => $rate) { echo '{text: "'.$rate['Description'].' ('.$code.')", value: "'.$code.'"},'; } ?>
                ]},
            {type: "input", name: "inpAmountPay", label: "Amount to Pay (USD)"},
            {type: "input", name: "inpAmountPurchase", label: "Amount to Purchase"},
            {type: "button", name: "btnPurchase", value: "Purchase Currency", disabled:true, width:"200"}

        ];

        dhxForm = a.attachForm(formStructure);
        dhxForm.attachEvent("onChange", dhxForm_onChange);
        dhxForm.attachEvent("onInputChange", dhxForm_onInputChange);
        dhxForm.attachEvent("onButtonClick", dhxForm_onButtonClick);
        dhxForm.attachEvent("onBlur", dhxForm_onBlur);
    }

    function dhxForm_onChange(name, value) {
        cLog('dhxForm_onChange(name='+name+', value='+value+')');

        if (name == 'selCurrency') {
            if (!empty(value)) {
                dhxForm.enableItem('btnPurchase');
            }
            else {
                dhxForm.disableItem('btnPurchase');
            }

            calcAmountPurchase();
        }
    };

    function dhxForm_onInputChange(name, value, form) {
        cLog('dhxForm_onInputChange(name='+name+', value='+value+', form='+form+')');

        if (!empty(dhxForm.getItemValue('selCurrency'))) {
            switch (name) {
                case 'inpAmountPay':
                    calcAmountPurchase();
                    break;
                case 'inpAmountPurchase':
                    calcAmountPay();
                    break;
            }
        }
    };

    function dhxForm_onBlur(name, value) {
        cLog('dhxForm_onBlur(name='+name+', value='+value+')');

        if (!empty(dhxForm.getItemValue('selCurrency'))) {
            switch (name) {
                case 'inpAmountPay':
                    calcAmountPurchase();
                    break;
                case 'inpAmountPurchase':
                    calcAmountPay();
                    break;
            }
        }
    };

    function dhxForm_onButtonClick(name) {
        cLog('dhxForm_onButtonClick(name='+name+')');

        var AmountPurchase = dhxForm.getItemValue('inpAmountPurchase');
        var Currency = dhxForm.getItemValue('selCurrency');
        if (name == 'btnPurchase' && !empty(AmountPurchase)) {
            var URL = '<?=base_url('index.php/home/order_insert/');?>?Currency='+Currency+'&AmountPurchase='+AmountPurchase;
            cLog('URL='+URL);
            var r = window.dhx4.ajax.getSync(URL);
            cLog('r='+r.xmlDoc.responseText);
            var obj = window.dhx4.s2j(r.xmlDoc.responseText);
            cLog('\\/ obj \\/');
            cLog(obj);
            if (obj != null && obj.status == 'ok') {
                dhxForm.setItemValue('inpAmountPurchase','');
                dhxForm.setItemValue('inpAmountPay','');
                dhxForm.setItemValue('selCurrency','');
                alert("Order placed successfully");
                dhtmlx.message({text: obj.message, expire:2000})
                return true;
            }
            else {
                dhtmlx.message({title: "Error",	type:"alert-error",	text:obj.message});
                return false;
            }
        }
    };

    function calcAmountPurchase() {
        cLog('calcAmountPurchase');

        var AmountPay = dhxForm.getItemValue('inpAmountPay');
        var Currency = dhxForm.getItemValue('selCurrency');
        var Rate = gCurrencies[Currency].Rate;

        if (AmountPay && Rate) {
            var AmountPurchase = AmountPay * Rate;
            dhxForm.setItemValue('inpAmountPurchase', AmountPurchase.toFixed(2));
        }
        else {
            dhxForm.setItemValue('inpAmountPurchase', '');
        }
    }

    function calcAmountPay() {
        cLog('calcAmountPay');

        var AmountPurchase = dhxForm.getItemValue('inpAmountPurchase');
        var Currency = dhxForm.getItemValue('selCurrency');
        var Rate = gCurrencies[Currency].Rate;

        if (AmountPurchase && Rate)  {
            var AmountPay = AmountPurchase / Rate;
            dhxForm.setItemValue('inpAmountPay', AmountPay.toFixed(2));
        }
    }

</script>

<body id="page1">
<div class="main">
    <!-- header -->
    <header>
        <div class="wrapper">
            <h1><a href="index.html" id="logo">Smart Biz</a></h1>
            <form id="search" action="" method="post">
                <div class="bg">
                    <input type="submit" class="submit" value="">
                    <input type="text" class="input">
                </div>
            </form>
        </div>
        <nav>
            <ul id="menu">
                <li class="alpha" id="menu_active"><a href="<?=base_url('index.php/home');?>"><span><span>Forex</span></span></a></li>
                <li><a href="<?=base_url('index.php/rates');?>"><span><span>Rates</span></span> </a></li>
                <li><a href="<?=base_url('index.php/orders');?>"><span><span>Orders</span></span></a></li>
                <li><a href="<?=base_url('index.php/database');?>"><span><span>Database</span></span></a></li>
                <li class="omega"><a href="<?=base_url('index.php/gui');?>"><span><span>API</span></span></a></li>
            </ul>
        </nav>
        <div class="wrapper">
            <div class="text">
                <span class="text1">Effective<span>business solutions</span></span>
                <a href="#" class="button">read more</a>
            </div>
        </div>
    </header><div class="ic">More Website Templates at TemplateMonster.com!</div>
    <!-- / header -->
    <!-- content -->
    <section id="content">
        <div class="wrapper">
            <div class="wrapper">
                <ul class="banners">
                    <li>
                        <a href="#"><img src="../images/page1_img1.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Company History</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img2.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Our Capabilities</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img3.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Where We Deliver</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img4.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Operations Consulting</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="wrapper">
                <div class="box bot pad_bot2">
                    <div class="pad">
                        <article class="col1">
                            <h2>Get the best Forex deal</h2>
                            <div style="height:300px" id="forexorder">

                            </div>
                        </article>
                        <article class="col2 pad_left1">
                            <h2>Latest projects</h2>
                            <div class="wrapper">
                                <div class="wrapper pad_bot1">
                                    <figure class="left marg_right1"><a href="#"><img src="../images/page1_img6.jpg" alt=""></a></figure>
                                    <p></p>
                                    <a href="#" class="marker"></a>
                                </div>
                                <div class="wrapper pad_bot1">
                                    <figure class="left marg_right1"><a href="#"><img src="../images/page1_img7.jpg" alt=""></a></figure>
                                    <p></p>
                                    <a href="#" class="marker"></a>
                                </div>
                                <div class="wrapper">
                                    <figure class="left marg_right1"><a href="#"><img src="../images/page1_img8.jpg" alt=""></a></figure>
                                    <p></p>
                                    <a href="#" class="marker"></a>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / content -->
</div>
<script type="text/javascript"> Cufon.now(); </script>
</body>
