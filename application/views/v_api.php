<body id="page1">
<div class="main">
    <!-- header -->
    <header>
        <div class="wrapper">
            <h1><a href="index.html" id="logo">Smart Biz</a></h1>
            <form id="search" action="" method="post">
                <div class="bg">
                    <input type="submit" class="submit" value="">
                    <input type="text" class="input">
                </div>
            </form>
        </div>
        <nav>
            <ul id="menu">
                <li class="alpha"><a href="<?=base_url('index.php/home');?>"><span><span>Forex</span></span></a></li>
                <li><a href="<?=base_url('index.php/rates');?>"><span><span>Rates</span></span> </a></li>
                <li><a href="<?=base_url('index.php/orders');?>"><span><span>Orders</span></span></a></li>
                <li><a href="<?=base_url('index.php/database');?>"><span><span>Database</span></span></a></li>
                <li class="omega" id="menu_active"><a href="<?=base_url('index.php/gui');?>"><span><span>API</span></span></a></li>
            </ul>
        </nav>
        <div class="wrapper">
            <div class="text">
                <span class="text1">Effective<span>business solutions</span></span>
                <a href="#" class="button">read more</a>
            </div>
        </div>
    </header><div class="ic">More Website Templates at TemplateMonster.com!</div>
    <!-- / header -->
    <!-- content -->
    <section id="content">
        <div class="wrapper">
            <div class="wrapper">
                <ul class="banners">
                    <li>
                        <a href="#"><img src="../images/page1_img1.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Company History</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img2.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Our Capabilities</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img3.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Where We Deliver</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img4.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Operations Consulting</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="wrapper">
                <div class="box bot pad_bot2">
                    <div class="pad">
                        <article>
                            <h2>API calls</h2>
                            <table>
                                <thead>
                                    <th colspan="2">API functions</th>
                                </thead>
                                <tr>
                                    <td>
                                        Get Rate by [ID]
                                    </td>
                                    <td>
                                        <?=base_url('index.php/api/rate/?id=[ID]')?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Get Rates
                                    </td>
                                    <td>
                                        <a href="<?=base_url('index.php/api/rates')?>" target="_new"><?=base_url('index.php/api/rates')?></a>
                                    </td>
                                </tr>
                                <thead>
                                <th colspan="2">Rate API functions</th>
                                </thead>
                                <tr>
                                    <td>
                                        Get All Rates
                                    </td>
                                    <td>
                                        <a href="<?=base_url('index.php/api/rates/rates_get_all')?>" target="_new"><?=base_url('index.php/api/rates/rates_get_all')?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Add a Rate
                                    </td>
                                    <td>
                                        <?=base_url('index.php/rates/rate_insert/');?>?Code=[Code]&Description=[Description]&Rate=[Rate]&Surcharge=[Surcharge]&Discount=[Discount];
                                    </td>
                                </tr>
                                <thead>
                                <th colspan="2">Order API functions</th>
                                </thead>
                                <tr>
                                    <td>
                                        Get All Orders
                                    </td>
                                    <td>
                                        <a href="<?=base_url('index.php/orders/orders_get_all')?>" target="_new"><?=base_url('index.php/orders/orders_get_all')?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Add an Order
                                    </td>
                                    <td>
                                        <?=base_url('index.php/home/order_insert/');?>?Currency=[Currency]&AmountPurchase=[AmountPurchase]
                                    </td>

                                </tr>
                            </table>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / content -->
</div>
<script type="text/javascript"> Cufon.now(); </script>
</body>
