<script type="text/javascript" charset="utf-8">
    var dhxLayout, a, b;
    var dhxDatabaseForm;

    var gCurrencies =<?php echo json_encode($ar_rates);?>;

    dhtmlxEvent(window, "load", function(){
        cLog('window.load');

        dhxLayout_init();
    })


    function dhxLayout_init() {
        cLog('dhxLayout_init()');

        dhxLayout = new dhtmlXLayoutObject({
            parent:"forexdatabase",
            pattern:"1C",
            offsets: {top: 0, right: 0, bottom: 0, left: 0},
            cells: [
                {
                    id: "a",
                    text: "Database",
                    collapse: false,
                    header: true,
                },
            ]
        });

        a = dhxLayout.cells('a');

        dhxLayout.setSizes();

        dhxForm_init();
    }

    function dhxForm_init() {
        var emailFormStructure = [
            {type: "settings", position: "label-left", labelWidth: "200", inputWidth: "200", offsetLeft:20},
            {type: "label", label: "Email"},
            {type: "input", name: "inpEmail", label: "Destination Email"},
            {type: "button", name: "btnSetEmail", value: "Set Email", width:"200"},
            {type: "label", label: "Database"},
            {type: "button", name: "btnClearRatesDB", value: "Clear Rates DB", width:"200"},
            {type: "button", name: "btnSeedRatesDB", value: "Seed Rates DB", width:"200"},
            {type: "input", name: "inpCode", label: "Code"},
            {type: "input", name: "inpDescription", label: "Description"},
            {type: "input", name: "inpRate", label: "Rate"},
            {type: "input", name: "inpSurcharge", label: "Surcharge"},
            {type: "input", name: "inpDiscount", label: "Discount"},
            {type: "button", name: "btnAddRate", value: "Add Rate", width:"200"}
        ];

        dhxDatabaseForm = a.attachForm(emailFormStructure);
        dhxDatabaseForm.attachEvent("onButtonClick", dhxDatabaseForm_onButtonClick);
    }

    function dhxDatabaseForm_onButtonClick(name) {
        cLog('dhxForm_onButtonClick(name='+name+')');
        var Email = dhxDatabaseForm.getItemValue('inpEmail');

        var Code = dhxDatabaseForm.getItemValue('inpCode');
        var Description = dhxDatabaseForm.getItemValue('inpDescription');
        var Rate = dhxDatabaseForm.getItemValue('inpRate');
        var Surcharge = dhxDatabaseForm.getItemValue('inpSurcharge');
        var Discount = dhxDatabaseForm.getItemValue('inpDiscount');

        if (name == 'btnSetEmail' && !empty(Email)) {
        }
        else if (name == 'btnClearRatesDB') {
            var URL = '<?=base_url('index.php/rates/cleartable');?>';
            var r = window.dhx4.ajax.getSync(URL);
            var obj = window.dhx4.s2j(r.xmlDoc.responseText);
            alert("Rates table cleared");
        }
        else if (name == 'btnSeedRatesDB') {
            var URL = '<?=base_url('index.php/rates/seedinitial');?>';
            var r = window.dhx4.ajax.getSync(URL);
            var obj = window.dhx4.s2j(r.xmlDoc.responseText);
            alert("Rates table seeded");
        }
        else if (name == 'btnAddRate' && !empty(Code) && !empty(Description) && !empty(Rate) && !empty(Surcharge)) {
            var URL = '<?=base_url('index.php/rates/rate_insert/');?>?Code='+Code+'&Description='+Description+'&Rate='+Rate+'&Surcharge='+Surcharge+'&Discount='+Discount;
            var r = window.dhx4.ajax.getSync(URL);
            dhxDatabaseForm.setItemValue('inpCode','');
            dhxDatabaseForm.setItemValue('inpDescription','');
            dhxDatabaseForm.setItemValue('inpRate','');
            dhxDatabaseForm.setItemValue('inpSurcharge','');
            dhxDatabaseForm.setItemValue('inpDiscount','');
            alert("Rate added");
        }
    };

</script>

<body id="page1">
<div class="main">
    <!-- header -->
    <header>
        <div class="wrapper">
            <h1><a href="index.html" id="logo">Smart Biz</a></h1>
            <form id="search" action="" method="post">
                <div class="bg">
                    <input type="submit" class="submit" value="">
                    <input type="text" class="input">
                </div>
            </form>
        </div>
        <nav>
            <ul id="menu">
                <li class="alpha"><a href="<?=base_url('index.php/home');?>"><span><span>Forex</span></span></a></li>
                <li><a href="<?=base_url('index.php/rates');?>"><span><span>Rates</span></span> </a></li>
                <li><a href="<?=base_url('index.php/orders');?>"><span><span>Orders</span></span></a></li>
                <li id="menu_active"><a href="<?=base_url('index.php/database');?>"><span><span>Database</span></span></a></li>
                <li class="omega"><a href="<?=base_url('index.php/gui');?>"><span><span>API</span></span></a></li>
            </ul>
        </nav>
        <div class="wrapper">
            <div class="text">
                <span class="text1">Effective<span>business solutions</span></span>
                <a href="#" class="button">read more</a>
            </div>
        </div>
    </header>
    <!-- / header -->
    <!-- content -->
    <section id="content">
        <div class="wrapper">
            <div class="wrapper">
                <ul class="banners">
                    <li>
                        <a href="#"><img src="../images/page1_img1.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Company History</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img2.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Our Capabilities</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img3.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Where We Deliver</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="../images/page1_img4.jpg" alt=""></a>
                        <div class="pad">
                            <p class="font1">Operations Consulting</p>
                            <p></p>
                            <a href="#" class="marker"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="wrapper">
                <div class="box bot pad_bot2">
                    <div class="pad">
                        <article>
                            <h2>Database Functions</h2>
                            <div style="height:600px" id="forexdatabase">

                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / content -->
</div>
<script type="text/javascript"> Cufon.now(); </script>
</body>
