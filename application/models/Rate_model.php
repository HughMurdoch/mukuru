<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Rate_model extends CI_Model {

    public $Code;
    public $Description;
    public $Rate;
    public $Surcharge;
    public $Discount;

    public function __construct() {
        parent::__construct();
    }

    public function get($id){
        return $this->db->get_where('rates', array('Code' => $id))->row();
    }

    public function get_all() {
		$this->db->order_by('Description', 'ASC');
        $query = $this->db->get('rates');
		if (count($query->result()) == 0) {
		    $this->seedinitial();
            $query = $this->db->get('rates');
        }
        return $query->result();
    }

    private function populateSeed($code, $description, $rate, $surcharge, $discount = 0)
    {
        $seed = new $this;
        $seed->Code = $code;
        $seed->Description = $description;
        $seed->Rate = $rate;
        $seed->Surcharge = $surcharge;
        $seed->Discount = $discount;
        return $seed;
    }

    public function cleartable() {
        $this->db->empty_table('rates');
    }

    public function seedinitial() {
        $this->db->empty_table('rates');

        $initialEntries = array (
            "zar" => $this->populateSeed('ZAR', 'South African Rands', 13.3054, 7.5),
            "gbp" => $this->populateSeed('GBP', 'British Pound', 0.651178, 5),
            "eur" => $this->populateSeed('EUR', 'Euro', 0.884872, 5, 0.02),
            "kes" => $this->populateSeed('KES', 'Kenyan Shilling', 103.860, 2.5)
        );


        foreach ($initialEntries as $key => $value) {
            $this->db->insert('rates', $value);
        }
    }

    public function insert($data) {
        $this->Code = $data['Code'];
        $this->Description = $data['Description'];
        $this->Rate = $data['Rate'];
        $this->Surcharge = $data['Surcharge'];
        if($data->Discount == "") {
            $this->Discount = 0;
        }
        else {
            $this->Discount = $data["Discount"];
        }

        $this->db->insert('rates', $this);
    }


	/*
    public function delete($id) {
        $this->db->delete('rates', array('id' => $id)); 
    }
	*/
}