<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    public $id;
    public $Currency;
    public $Rate;
    public $Surcharge;
    public $AmountPurchase;
    public $AmountPay;
    public $AmountSurcharge;
    public $AmountDiscount;

    public function __construct() {
        parent::__construct();
    }

    public function get($id){
        return $this->db->get_where('orders', array('id' => $id))->row();
    }

    public function get_all() {
		$this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
		
        return $query->result();
    }

    public function insert($data) {
        $this->Currency = $data['Currency'];
        $this->Rate = $data['Rate'];
        $this->Surcharge = $data['Surcharge'];
        $this->AmountPurchase = $data['AmountPurchase'];
        $this->AmountPay = $data['AmountPay'];
        $this->AmountSurcharge = $data['AmountSurcharge'];
        $this->AmountDiscount = $data['AmountDiscount'];

        $this->db->insert('orders', $this);
    }
	
	/*
    public function delete($id) {
        $this->db->delete('orders', array('id' => $id)); 
    }
	*/
}