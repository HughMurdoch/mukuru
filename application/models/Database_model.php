<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Database_model extends CI_Model {

    public $Code;
    public $Description;
    public $Rate;
    public $Surcharge;
    public $Discount;

    public function __construct() {
        parent::__construct();
    }

    public function getemail(){
        return $this->db->get('database')->row();
    }

    private function populateSeed($code, $description, $rate, $surcharge, $discount = 0)
    {
        $seed = new $this;
        $seed->Code = $code;
        $seed->Description = $description;
        $seed->Rate = $rate;
        $seed->Surcharge = $surcharge;
        $seed->Discount = $discount;
        return $seed;
    }

    public function seedinitial() {
        $this->db->empty_table('rates');

        $initialEntries = array (
            "zar" => $this->populateSeed('ZAR', 'South African Rands', 13.3054, 7.5),
            "gbp" => $this->populateSeed('GBP', 'British Pound', 0.651178, 5),
            "eur" => $this->populateSeed('EUR', 'Euro', 0.884872, 5, 0.02),
            "kes" => $this->populateSeed('KES', 'Kenyan Shilling', 103.860, 2.5)
        );


        foreach ($initialEntries as $key => $value) {
            $this->db->insert('rates', $value);
        }
    }

    public function cleartable() {
        $this->db->empty_table('rates');
    }

	/*
    public function delete($id) {
        $this->db->delete('rates', array('id' => $id)); 
    }
	*/
}